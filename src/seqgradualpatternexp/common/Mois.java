package seqgradualpatternexp.common;

/**
 *
 * @author fotso
 */
public enum Mois {
    Jen,
    Feb,
    Mar,
    Apr,
    May,
    Jun,
    Jul,
    Aug,
    Sep,
    Oct,
    Nov,
    Dec
}
